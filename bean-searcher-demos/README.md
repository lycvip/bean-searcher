# Bean Searcher 在各框架内的使用案例

* [SpringBoot 案例](./spring-boot-demo)
  - 使用 H2 数据库，无需任何配置即可启动体验
  - JDK 8+
* [SpringBoot 案例（使用 JDK9+ 的模块系统）](./spring-boot-demo-jdkmods)
  - 使用 H2 数据库，无需任何配置即可启动体验
  - JDK 9+
* [SpringBoot 案例（使用 mysql 驱动）](./spring-boot-demo-mysql)
  - 需要配置数据库连接参数
  - JDK 8+
* [Grails 案例](./grails-demo)（无需配置数据库）
  - 使用 H2 数据库，无需任何配置即可启动体验
  - JDK 8+
